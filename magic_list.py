from dataclasses import dataclass


class MagicList(list):

    def __init__(self, cls_type=None):
        self.my_class = cls_type

    def __setitem__(self, key, value):
        if self.my_class:
            value = self.my_class()
        try:
            super(MagicList, self).__setitem__(key, value)
        except IndexError as e:
            if key == len(self):
                self.append(value)
            else:
                raise e

    def __getitem__(self, item):
        try:
            super(MagicList, self).__getitem__(item)
        except IndexError as e:
            if item == len(self):
                self.append(self.my_class())
                return self
            raise e


@dataclass
class Person:
    age: int = 1


x = MagicList()
x[0] = 5
print(x)
# x[4] = 5
# print(x)

a = MagicList(cls_type=Person)
a[0].age = 5
print(a)
