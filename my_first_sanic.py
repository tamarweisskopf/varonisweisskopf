from sanic import Sanic
from sanic import response
app = Sanic("My First Sanic App")


@app.route("/get_normal", methods=['POST'])
def on_post(request):
    my_input = request.json
    noraml_input = {x.get('name'): x.get([key for key in x.keys() if 'Val' in key][0]) for x in my_input}
    return response.json({"noraml_input": noraml_input})

# debug logs enabled with debug = True
app.run(host='localhost', port=8000, debug=True)
